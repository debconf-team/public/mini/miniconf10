# Makefile -- helper for the site
#
# Copyright (c) martin f. krafft <phd ät martin-krafft.net>
# Released under the terms of the Artistic Licence 2.0
#
# Part of the source code for the site http://martin-krafft.net/phd/
#

all:
	r2w -w
.PHONY: all

clean:
	rm -rf *.html
.PHONY: clean
