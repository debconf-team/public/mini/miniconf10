<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<# copyright="""
  template.txt : site template (for rest2web)

  Copyright © martin f. krafft <phd ät martin-krafft.net>
  Released under the terms of the Creative Commons Attribution-
  NonCommercial-ShareAlike 2.5 Licence:
    http://creativecommons.org/licenses/by-nc-sa/2.5/

  Part of the source code for the site http://distrosummit.org/
  """
#>
<head>
  <title><% title %> - <% thispage['uservalues']['site_subtitle'] %></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="imagetoolbar" content="no" />
  <link rel="start" href="./<% path_to_root %>" title="Front page" />
  <link rel="stylesheet" media="screen" type="text/css"
    href="<% path_to_root %>css/noleftbar.css" title="noleftbar"/>
  <link rel="alternate stylesheet" media="all" type="text/css"
    href="<% path_to_root %>css/base.css" title="basic" />
  <link rel="stylesheet" media="print" type="text/css"
    href="<% path_to_root %>css/print.css" />
</head>
<body>
  <div id="body">
    <div id="top">
      <div id="header">
        <p class="title">
          <% thispage['uservalues']['site_title'] %><br/>
          <% thispage['uservalues']['site_title2'] %>
        </p>
        <p class="subtitle">
          <% thispage['uservalues']['site_subtitle'] %>
        </p>
      </div>
      <hr class="border" />
      <#
        def category_list(indextree):
          l = []
          uv = indextree.get('uservalues', {})
          if uv.has_key('category_name') \
            or uv.has_key('category_prio'):
    
            prio = uv.get('category_prio', 'default')
            if prio == 'default': prio = 50
    
            l.append((int(prio), 
                      uv.get('category_name', indextree['link-title']),
                      indextree['target'],
                      indextree['thispage']))
    
            if indextree['pages'] is not None:
              for p in indextree['pages']:
                l += category_list(p)
    
          l.sort()
          return l
    
        def is_current_category(page, category):
          p = page
          while p is not None:
            uv = p.get('uservalues', {})
            cat_name = uv.get('category_name', p['link-title'])
            if cat_name == category:
              # only return true for the site root if the current page is the site
              # root. if we ended up at the site root because no matching category
              # was found, return false
              return p['target'] == page['target'] or p.get('parent') is not None
            p = p.get('parent', None)
          return False
    
        l = category_list(indextree)
        if len(l) > 0: print '<ul id="global-links">'
        from sys import stdout
        for i in l:
          stdout.write('<li')
          if is_current_category(thispage, i[1]):
            stdout.write(' class="current"')
          stdout.write('><a href="%s">%s</a>' % (i[2], i[1]))
          if is_current_category(thispage, i[1]):
            stdout.write('<span class="textmode"> (you are here)</span>')
          print '</li>'
        if len(l) > 0: print '</ul>'
      #>
    </div>
    <div id="content">
      <% body %>
    </div>
    <div id="left">
      <div id="toc">
        <h1>Table of contents</h1>
        <#
          for page in sidebar(indextree, '<ul>', '</ul>'):
            if page.get('uservalues', {}).get('no_toc_entry', False):
              continue
            val = page['link-title']
            link = page['target']
            from sys import stdout
            if page['thispage']:
              stdout.write('<li class="current">')
            else:
              stdout.write('<li><a href="%s">' % link)
            stdout.write(val)
            if not page['thispage']:
              stdout.write('</a>')
            else:
              stdout.write('<span class="textmode"> (you are here)</span>')
            print '</li>'
        #>
      </div>
      <div class="stretch"><!-- --></div>
    </div>
    <div id="bottom">
      <hr class="border" />
      <p>This page was last modified <% modtime %>.</p>
      <div id="colophon">
        <p>
          This webpage wouldn't be possible without <a
          href="http://debian.org/">Debian</a>, <a
          href="http://python.org/">Python</a>, <a
          href="http://voidspace.org.uk/python/rest2web/">rest2web</a>, <a
          href="http://httpd.apache.org/">Apache</a>, and other
          excellent pieces of <a href="http://fsf.org/">Free Software</a>.
        </p>
        <p>
          <a href="http://validator.w3.org/check/referer">
            <img src="http://martin-krafft.net/img/xhtml1.png" alt="Valid XHTML 1.0 Strict" /></a>
          <a href="http://jigsaw.w3.org/css-validator/check/referer/">
            <img src="http://martin-krafft.net/img/css.png" alt="Valid CSS" /></a>
        </p>
      </div>
    </div>
  </div>
</body>
</html>
